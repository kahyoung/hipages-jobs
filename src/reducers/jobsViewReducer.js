import jobsViewActions from '../actions/jobsViewActions';
import JobStatus from '../enums/JobStatus';

const jobsViewReducer = (state=JobStatus.inProgress, action) => {
    switch(action.type) {
        case jobsViewActions.changeJobView:
            return action.payload;
        default:
            return state;
    }
};

export default jobsViewReducer;