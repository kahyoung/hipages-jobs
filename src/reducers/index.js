import { combineReducers } from 'redux';

import jobsViewReducer from './jobsViewReducer';
import jobsReducer from './jobsReducer';

export default combineReducers({
    jobsView: jobsViewReducer,
    jobs: jobsReducer
});