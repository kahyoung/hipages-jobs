# Reducers
All reducers for [Redux](https://redux.js.org) should exist as its own file, in this directory.

If adding any new Reducers, it is important to make sure that the `index.js` file in this directory imports the new reducer. 
This is required, as the main `store.js` file does not look at individual reducers, only the main `index.js` file in
this directory.

For more information, see [Reducers](https://redux.js.org/basics/reducers).