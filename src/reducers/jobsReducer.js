import moment from 'moment';
import jobsActions from '../actions/jobsActions';

import JobStatus from '../enums/JobStatus';

const jobsReducer = (state={loading: true, data: []}, action) => {
    switch (action.type) {
        case jobsActions.fetchJobsPending:
            return {...state, loading: true};
        case jobsActions.fetchJobsFulfilled:
            return {...state, loading: false, data: action.payload};
        case jobsActions.openJob:
            let jobs = [...state.data];
            let job = jobs.find(job => job.jobId === action.payload.jobId);
            job.status = JobStatus.inProgress;
            job.postedDate = moment().format('YYYY-MM-DD');

            return {...state, data: jobs};
        case jobsActions.closeJob:
            jobs = [...state.data];
            job = jobs.find(job => job.jobId === action.payload.jobId);
            job.status = JobStatus.closed;

            return {...state, data: jobs};
        default:
            return state;
    }
};

export default jobsReducer;