# Enums

Any non-action enums exist in this directory.

Anything that is common, like status, should exist here. This keeps all enums in one centralised place, making it easier to find 
and refactor if need be in the future.