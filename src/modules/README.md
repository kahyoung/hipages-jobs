# Modules
A module is a collection of components. Generally, a module represents a whole Page of this application. As such, they 
should rarely, if ever, have styles of its own - but rather rely on the style and layouts of the components which make up 
each module.