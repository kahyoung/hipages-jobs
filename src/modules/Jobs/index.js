import React, { Component } from 'react';

// Redux
import { connect } from 'react-redux';
import { fetchJobs, openJob, closeJob } from '../../actions/jobsActions';
import { changeJobsView } from "../../actions/jobsViewActions";

import JobStatus from '../../enums/JobStatus';

import Page from '../../components/Page';
import Section from '../../components/Section';
import SegmentedControl from '../../components/SegmentedControl';
import Segment from '../../components/SegmentedControl/Segment';
import JobCard from '../../components/JobCard';
import ContextOptionModel from "../../components/ContextMenu/ContextOption/model";
import CardHeader from "../../components/Card/CardHeader";
import Card from "../../components/Card";

document.title = 'Jobs | hipages';

/**
 * The Jobs page, which contains a list of Open and Closed jobs
 */
class Jobs extends Component {
    constructor(props) {
        super(props);

        this.isActiveSegment = this.isActiveSegment.bind(this);
        this.openJob = this.openJob.bind(this);
        this.closeJob = this.closeJob.bind(this);
        this.changeJobsViewToInProgress = this.changeJobsViewToInProgress.bind(this);
        this.changeJobsViewToClosed = this.changeJobsViewToClosed.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(fetchJobs());
    }

    /**
     * Determines if the given status is the current in-view segment
     *
     * @param {string} status - The status to be checked
     */
    isActiveSegment(status) {
        return status === this.props.jobsView;
    }

    /**
     * Changes the current segment's job view to be In Progress
     */
    changeJobsViewToInProgress() {
        this.props.dispatch(changeJobsView(JobStatus.inProgress));
    }

    /**
     * Changes the current segment's job view to be Closed
     */
    changeJobsViewToClosed() {
        this.props.dispatch(changeJobsView(JobStatus.closed));
    }

    /**
     * Marks the job's status as open
     *
     * @param {object} job - The job to open
     */
    openJob(job) {
        this.props.dispatch(openJob(job));
    }

    /**
     * Marks the job's status as closed
     *
     * @param {object} job - The job to open
     */
    closeJob(job) {
        this.props.dispatch(closeJob(job));
    }

    /**
     * Determines if we're currently showing open jobs
     *
     * @return {boolean}
     */
    isShowingOpenJobs() {
        return this.props.jobsView.toLowerCase() === JobStatus.inProgress.toLowerCase();
    }

    render() {
        let jobSections = this.props.jobs.map(job => {
            let actions = [];

            if (job.status.toLowerCase() === JobStatus.inProgress.toLowerCase()) {
                actions.push(new ContextOptionModel('Close job', this.closeJob.bind(this, job)));
            }

            if (job.status.toLowerCase() === JobStatus.closed.toLowerCase()) {
                actions.push(new ContextOptionModel('Open job', this.openJob.bind(this, job)));
            }

            return (
                <Section key={job.jobId}>
                    <JobCard job={job} actions={actions}/>
                </Section>
            );
        });

        // Show a special message if there's no jobs in this view
        if (jobSections.length === 0) {
            let state = this.isShowingOpenJobs() ? 'open' : 'closed';

            jobSections = (
                <Section>
                    <Card transparent dashed>
                        <CardHeader subtitle={`No ${state} jobs`}/>
                    </Card>
                </Section>
            );
        }

        return (
            <Page>
                <Section>
                    <SegmentedControl>
                        <Segment active={this.isActiveSegment(JobStatus.inProgress)} onClick={this.changeJobsViewToInProgress}>Open Jobs</Segment>
                        <Segment active={this.isActiveSegment(JobStatus.closed)} onClick={this.changeJobsViewToClosed}>Closed Jobs</Segment>
                    </SegmentedControl>
                </Section>
                {jobSections}
            </Page>
        );
    }
}

const mapStateToProps = (store) => {
    // We should only match jobs that match the current job view
    let jobs = store.jobs.data.filter(job => job.status.toLowerCase() === store.jobsView.toLowerCase());
    jobs.sort((a, b) => {
        return new Date(b.postedDate) - new Date(a.postedDate);
    });

    return {
        jobs: jobs,
        jobsView: store.jobsView
    }
};

export default connect(mapStateToProps)(Jobs);