import React from "react";
import { render } from 'enzyme';

import Section from './';

it('renders without crashing', () => {
    render(<Section>Test</Section>);
});