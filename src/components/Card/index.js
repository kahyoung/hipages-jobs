import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Card.css';

/**
 * A Card is a container of content, generally used in the context of a Collection - holding more information than a simple list would
 *
 * It has 2 main components, a {@link CardHeader} and a {@link CardContent}
 */
const Card = ({dashed, transparent, children}) => (
    <div className={classNames('card', {'card--dashed': dashed, 'card--transparent': transparent})}>
        {children}
    </div>
);

Card.propTypes = {
    dashed: PropTypes.bool,
    transparent: PropTypes.bool,
    children: PropTypes.node
};

export default Card;