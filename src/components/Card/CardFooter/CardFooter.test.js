import React from "react";
import { shallow } from 'enzyme';

import CardFooter from "./";

it('renders without crashing', () => {
    shallow(<CardFooter/>);
});

it('can render children', () => {
    let wrapper = shallow(<CardFooter><div className="test">Test</div></CardFooter>);
    expect(wrapper.find('.test').length).toBe(1);
});