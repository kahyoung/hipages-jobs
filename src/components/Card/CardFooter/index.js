import React from 'react';
import PropTypes from 'prop-types';

import './CardFooter.css';

/**
 * An CardFooter is the element at the bottom of a {@link Card}
 *
 * It adds additional spacing for presentation purposes
 */
const CardFooter = ({children}) => (
    <div className="card-footer">{children}</div>
);

CardFooter.propTypes = {
    children: PropTypes.node,
};

export default CardFooter;