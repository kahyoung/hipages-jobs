import React from 'react';
import PropTypes from 'prop-types';

import './CardContent.css';

/**
 * The main content of a card. Highly flexible component which presents any content that has been passed into it as children
 */
const CardContent = ({title, children}) => {
    let contentTitle = null;
    if (title) {
        contentTitle = <h5 className="card-content__title">{title}</h5>;
    }

    let cardContent = null;
    if(children) {
        cardContent = <div className="card-content__content">{children}</div>;
    }

    return (
        <div className="card-content">
            {contentTitle}
            {cardContent}
        </div>
    );
};

CardContent.propTypes = {
    title: PropTypes.string,
    children: PropTypes.node
};

export default CardContent;