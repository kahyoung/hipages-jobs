import React from "react";
import { render } from 'enzyme';

import CardContent from './';

it('renders without crashing', () => {
    render(<CardContent />);
});

it('can render a title', () => {
    let header = render(<CardContent />);
    expect(header.find('.card-content__title').length).toBe(0);

    header = render(<CardContent title="Title" />);
    expect(header.find('.card-content__title').length).toBe(1);
});