import React from 'react';
import PropTypes from 'prop-types';

import './CardHeader.css';
import ContextMenu from '../../ContextMenu';

import ContextOptionModel from '../../ContextMenu/ContextOption/model';

const CardHeader = ({title, subtitle, actions}) => {
    let headerTitle = null;
    if (title) {
        headerTitle = <h3 className="card-header__title">{title}</h3>;
    }

    let headerSubtitle = null;
    if (subtitle) {
        headerSubtitle = <h5 className="card-header__subtitle">{subtitle}</h5>;
    }

    let headerActions = null;
    if (actions) {
        headerActions = <ContextMenu className="card-header__context-menu" actions={actions} />;
    }

    return (
        <div className="card-header">
            {headerTitle}
            {headerSubtitle}
            {headerActions}
        </div>
    );
};

CardHeader.propTypes = {
    title: PropTypes.string,
    subtitle: PropTypes.string,
    actions: PropTypes.arrayOf(PropTypes.instanceOf(ContextOptionModel))
};

export default CardHeader;