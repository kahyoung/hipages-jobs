import React from "react";
import { render } from 'enzyme';

import CardHeader from './';

it('renders without crashing', () => {
    render(<CardHeader />);
});

it('can render a title', () => {
    let header = render(<CardHeader />);
    expect(header.find('.card-header__title').length).toBe(0);

    header = render(<CardHeader title="Title" />);
    expect(header.find('.card-header__title').length).toBe(1);
});

it('can render a subtitle', () => {
    let header = render(<CardHeader />);
    expect(header.find('.card-header__subtitle').length).toBe(0);

    header = render(<CardHeader subtitle="Subtitle" />);
    expect(header.find('.card-header__subtitle').length).toBe(1);
});