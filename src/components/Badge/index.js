import React from 'react';
import PropTypes from 'prop-types';

import './Badge.css';

/**
 * An Badge is an element which contains text surrounded by a color
 */
const Badge = ({children}) => (
    <div className="badge">{children}</div>
);

Badge.propTypes = {
    children: PropTypes.string.isRequired
};

export default Badge;