import React from "react";
import { render } from 'enzyme';

import Badge from './';

it('renders without crashing', () => {
    render(<Badge>HIRED</Badge>);
});