import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';

import './Link.css';

/**
 * Basic Link component that creates an element that looks like a link
 *
 * If a 'to' parameter is provided, it will render as an anchor tag with an href
 * Otherwise, it'll render a button that looks like an a tag (for semantic purposes)
 */
const Link = ({to, type = 'default', className, onClick, children}) => {
    let link = null;
    className = classNames('link', `link--${type}`, className);

    if (to) {
        link = <a href={to} className={className} onClick={onClick}>{children}</a>
    } else {
        link = <button className={className} onClick={onClick}>{children}</button>
    }

    return link;
};

Link.propTypes = {
    to: PropTypes.string,
    type: PropTypes.oneOf(['main', 'default']),
    className: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.node
};

// noinspection JSUnusedGlobalSymbols
export default Link