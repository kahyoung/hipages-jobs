import React from 'react';
import PropTypes from 'prop-types';

import Link from '../';

/**
 * A large link, mainly used for Actions (such as View Details)
 */
const MainLink = ({to, children}) => (
    <Link to={to} type="main">{children}</Link>
);

MainLink.propTypes = {
    to: PropTypes.string,
    children: PropTypes.node
};

export default MainLink;

