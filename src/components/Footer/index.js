import React from 'react';
import PropTypes from 'prop-types';

import './Footer.css';

/**
 * Footer component that is constantly at the bottom of the whole app
 *
 * Takes a title (generally the copyright text) and children (generally {@link FooterLink}
 */
const Footer = ({title, children}) => {
    let footerTitle = null;
    if (title) {
        footerTitle = <div className="footer__title">{title}</div>;
    }

    let footerLinks = null;
    if (children) {
        footerLinks = (
            <div className="footer__links">{children}</div>
        );
    }
    return (
        <div className="footer">
            {footerTitle}
            {footerLinks}
        </div>
    );
};

Footer.propTypes = {
    title: PropTypes.string,
    children: PropTypes.node
};

export default Footer;