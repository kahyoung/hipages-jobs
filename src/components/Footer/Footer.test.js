import React from "react";
import { shallow } from 'enzyme';

import Footer from './';

it('renders without crashing', () => {
    shallow(<Footer/>);
});

it('renders a title', () => {
    let wrapper = shallow(<Footer title="Test"/>);
    expect(wrapper.find('.footer__title').length).toBe(1);
});

it('renders links', () => {
    let wrapper = shallow(<Footer/>);
    expect(wrapper.find('.footer__links').length).toBe(0);

    wrapper = shallow(<Footer><div>Test</div></Footer>);
    expect(wrapper.find('.footer__links').length).toBe(1);
});