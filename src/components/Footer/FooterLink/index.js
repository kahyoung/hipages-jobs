import React from 'react';
import PropTypes from 'prop-types';

import Link from '../../Link';

import './FooterLink.css';

/**
 * A special link that exists in a Footer
 */
const FooterLink = ({to, children}) => (
    <Link to={to} className="footer-link">{children}</Link>
);

FooterLink.propTypes = {
    to: PropTypes.string,
    children: PropTypes.node
};

export default FooterLink;