import React from "react";
import { render } from 'enzyme';

import Avatar from "./";

it('renders without crashing', () => {
    render(<Avatar image="https://assets.homeimprovementpages.com.au/images/hui/avatars/a.png"/>);
});

it('can render a text badge', () => {
    let avatar = render(<Avatar image="https://assets.homeimprovementpages.com.au/images/hui/avatars/a.png" />);
    expect(avatar.find('.avatar__text').length).toBe(0);

    avatar = render(<Avatar image="https://assets.homeimprovementpages.com.au/images/hui/avatars/a.png" text="HIRED"/>);
    expect(avatar.find('.avatar__text').length).toBe(1);
});