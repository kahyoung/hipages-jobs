import React from 'react';
import PropTypes from 'prop-types';

import './Avatar.css';

import Badge from '../Badge';

/**
 * An Avatar is an element which contains a photo of a user/entity along with a text badge under it
 */
const Avatar = ({image, alt, text}) => {
    let avatarText = null;
    if (text) {
        avatarText = (
            <div className="avatar__text">
                <Badge>{text}</Badge>
            </div>
        );
    }

    return (
        <div className="avatar">
            <div className="avatar__image">
                <img className="avatar__image__image" src={image} alt={alt} />
            </div>
            {avatarText}
        </div>
    )
};

Avatar.propTypes = {
    image: PropTypes.string.isRequired,
    alt: PropTypes.string,
    text: PropTypes.string
};

export default Avatar;