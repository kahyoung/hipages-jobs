import React from "react";
import { render } from 'enzyme';

import BreakLine from './';

it('renders without crashing', () => {
    render(<BreakLine/>);
});

it('can render text within the breakline', () => {
    let breakLine = render(<BreakLine/>);
    expect(breakLine.find('.break-line__text').length).toBe(0);

    breakLine = render(<BreakLine>In Progress</BreakLine>);
    expect(breakLine.find('.break-line__text').length).toBe(1);
});

it('can take different color text', () => {
    let breakLine = render(<BreakLine textColor="green">In Progress</BreakLine>);
    expect(breakLine.find('.break-line__text--green').length).toBe(1);

    breakLine = render(<BreakLine textColor="red">In Progress</BreakLine>);
    expect(breakLine.find('.break-line__text--red').length).toBe(1);
});