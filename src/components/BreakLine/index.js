import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';

import './BreakLine.css';

/**
 * Renders a straight horizontal line, with an optional text in the center of the line
 */
const BreakLine = ({textColor = 'black', children}) => {

    let text = null;
    if (children) {
        let className = classNames(['break-line__text', `break-line__text--${textColor}`]);
        text = <span className={className}>{children}</span>;
    }

    return (
        <div className="break-line">
            {text}
        </div>
    );
};

BreakLine.propTypes = {
    children: PropTypes.string,
    textColor: PropTypes.oneOf(['green', 'red'])
};

export default BreakLine;