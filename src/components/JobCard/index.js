import React, { Component } from 'react';
import PropTypes from 'prop-types';
import pluralize from 'pluralize';
import moment from 'moment';

import JobStatus from '../../enums/JobStatus';

import Avatar from '../Avatar';
import Card from '../Card';
import CardHeader from '../Card/CardHeader';
import CardContent from '../Card/CardContent';
import CardFooter from '../Card/CardFooter';
import BreakLine from '../BreakLine';
import MainLink from '../Link/MainLink';

/**
 * A variant of a {@link Card} which is used in the Jobs module
 *
 * It is a simplified version of a {@link Card}, which creates its own template based purely off of a given job
 */
class JobCard extends Component {
    constructor(props) {
        super(props);

        this.getStatusColor.bind(this);
        this.getStatusText.bind(this);
        this.isClosed.bind(this);
        this.getConnectedMessage.bind(this);
    }
    /**
     * Gets the matching color for the Job's status
     *
     * It converts all text to lowercase in order to make sure that we get consistent results, regardless of any casing
     * differences from the API
     *
     * @return {string|null}
     */
    getStatusColor() {
        let jobStatus = this.props.job.status;

        switch (jobStatus.toLowerCase()) {
            case JobStatus.inProgress.toLowerCase():
                return 'green';
            case JobStatus.closed.toLowerCase():
                return 'red';
            default:
                return null;
        }
    }

    /**
     * Cleans the text of the Job's Status to handle any typos from the API
     *
     * @return {string | null}
     */
    getStatusText() {
        let jobStatus = this.props.job.status;

        switch(jobStatus.toLowerCase()) {
            case JobStatus.inProgress.toLowerCase():
                return 'In Progress';
            case JobStatus.closed.toLowerCase():
                return 'Closed';
            default:
                return null;
        }
    }

    /**
     * Determines if the given job status is a closed status
     *
     * @return {boolean}
     */
    isClosed() {
        return this.getStatusText() === JobStatus.closed;
    }

    /**
     * Generates the message for the Card content, depending on the amount of businesses that the job is connected to
     *
     * If the job is closed, then the message will be in past tense
     *
     * @return {string}
     */
    getConnectedMessage() {
        let businesses = this.props.job.connectedBusinesses || [];
        let isClosed = this.isClosed();

        if (businesses.length === 0) {
            return isClosed ? "Didn't connect with any businesses" : 'Connecting you with businesses';
        }

        let hiredBusinesses = businesses.filter(business => business.isHired);

        if (hiredBusinesses.length === 0) {
            return `Connected you to ${businesses.length} ${pluralize('business', businesses.length)}`;
        } else {
            return `You ${isClosed ? '' : 'have '}hired ${hiredBusinesses.length} ${pluralize('business', hiredBusinesses.length)}`;
        }
    }

    render() {
        let job = this.props.job;
        let businesses = job.connectedBusinesses || [];

        // It'd be nicer to have the hired business' first in the list
        businesses.sort((a, b) => {
            if (a.isHired === b.isHired) {
                return 0;
            } else if (a.isHired) {
                return -1;
            } else {
                return 1;
            }
        });

        let connectedBusinesses = businesses.map(business => {
            return <Avatar key={business.businessId} image={business.thumbnail} alt="Connected Business" text={business.isHired ? 'HIRED' : null}/>;
        });

        if (connectedBusinesses.length === 0) {
            connectedBusinesses = null;
        }

        return (
            <Card>
                <CardHeader title={job.category} subtitle={moment(job.postedDate).format('Do MMMM YYYY')} actions={this.props.actions}/>
                <BreakLine textColor={this.getStatusColor()}>{this.getStatusText()}</BreakLine>
                <CardContent title={this.getConnectedMessage()}>
                    {connectedBusinesses}
                </CardContent>
                <CardFooter>
                    <MainLink>View Details</MainLink>
                </CardFooter>
            </Card>
        );
    }
}

JobCard.propTypes = {
    job: PropTypes.object.isRequired
};

export default JobCard;