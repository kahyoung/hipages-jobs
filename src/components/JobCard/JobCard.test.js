import React from "react";
import { shallow } from 'enzyme';

import JobStatus from '../../enums/JobStatus';

import JobCard from './';
import Avatar from '../Avatar';

it('renders without crashing', () => {
    shallow(<JobCard job={getJobs()[0]}/>);
});

it('can render a list of avatars for each connected business', () => {
    let jobCard = shallow(<JobCard job={getJobs()[0]}/>);
    expect(jobCard.find(Avatar).length).toBe(4);
});

it('can clean a job status in case of any typos', () => {
    let job = getJobs()[0];
    job.status = 'in progress';

    let jobCard = shallow(<JobCard job={job}/>).instance();
    expect(jobCard.getStatusText()).toBe(JobStatus.inProgress);

    job.status = 'IN PROGRESS';
    jobCard = shallow(<JobCard job={job}/>).instance();
    expect(jobCard.getStatusText()).toBe(JobStatus.inProgress);

    job.status = 'In PrOgReSs';
    jobCard = shallow(<JobCard job={job}/>).instance();
    expect(jobCard.getStatusText()).toBe(JobStatus.inProgress);

    job.status = 'closed';
    jobCard = shallow(<JobCard job={job}/>).instance();
    expect(jobCard.getStatusText()).toBe(JobStatus.closed);

    job.status = 'CLOSED';
    jobCard = shallow(<JobCard job={job}/>).instance();
    expect(jobCard.getStatusText()).toBe(JobStatus.closed);

    job.status = 'cLoSeD';
    jobCard = shallow(<JobCard job={job}/>).instance();
    expect(jobCard.getStatusText()).toBe(JobStatus.closed);
});

it('can get the correct color for a given status', () => {
    let job = getJobs()[0];
    job.status = JobStatus.inProgress;

    let jobCard = shallow(<JobCard job={job}/>).instance();
    expect(jobCard.getStatusColor()).toBe('green');

    job.status = JobStatus.closed;
    jobCard = shallow(<JobCard job={job}/>).instance();
    expect(jobCard.getStatusColor()).toBe('red');
});

it('can get the correct connected businesses message', () => {
    let job = getJobs()[0];
    let jobCard = shallow(<JobCard job={job}/>).instance();
    expect(jobCard.getConnectedMessage()).toBe('You have hired 2 businesses');

    job.connectedBusinesses = [job.connectedBusinesses[0]];
    job.connectedBusinesses[0].isHired = false;
    jobCard = shallow(<JobCard job={job}/>).instance();
    expect(jobCard.getConnectedMessage()).toBe('Connected you to 1 business');

    job.connectedBusinesses = null;
    jobCard = shallow(<JobCard job={job}/>).instance();
    expect(jobCard.getConnectedMessage()).toBe('Connecting you with businesses');

    // Past tense
    job = getJobs()[0];
    job.status = JobStatus.closed;
    jobCard = shallow(<JobCard job={job}/>).instance();
    expect(jobCard.getConnectedMessage()).toBe('You hired 2 businesses');

    job.connectedBusinesses = null;
    jobCard = shallow(<JobCard job={job}/>).instance();
    expect(jobCard.getConnectedMessage()).toBe("Didn't connect with any businesses");
});

function getJobs() {
    return [
        {
            "jobId": 423421,
            "category": "Electricians",
            "postedDate": "2017-04-13",
            "status": "In Progress",
            "connectedBusinesses": [
                {
                    "businessId": 203213,
                    "thumbnail": "https://assets.homeimprovementpages.com.au/images/hui/avatars/a.png",
                    "isHired": false
                },
                {
                    "businessId": 205434,
                    "thumbnail": "https://assets.homeimprovementpages.com.au/images/hui/avatars/p.png",
                    "isHired": true
                },
                {
                    "businessId": 414324,
                    "thumbnail": "https://assets.homeimprovementpages.com.au/images/hui/avatars/i.png",
                    "isHired": false
                },
                {
                    "businessId": 324353,
                    "thumbnail": "https://assets.homeimprovementpages.com.au/images/hui/avatars/g.png",
                    "isHired": true
                }
            ],
            "detailsLink": "/jobs/423421"
        }
    ];
}