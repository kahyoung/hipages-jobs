# Components
This folder contains all the base level components that are used throughout the application.

Each component should be self explanatory, or have documentation to explain it.

The structure of this directory is as follows:

> {Component Name}

Each Component exists within a folder which is the name of the component.

Inside each folder, you'll find an `index.js` which contains the actual component, as well as any associating files (`test.js`, `style.css`, etc).

> {Component Name} > {Nested Component}

Any nested components will exist on a deeper level of a component.

Generally, most components should be self-standing. *However*, some components (such as NavLink) can only exist within another component.
In these cases, these special dependant components **must** exist within the component that it depends on. This makes it easier to understand 
each component conceptually, as it provides a clear, semantic layout of where each component sits.