import React from "react";
import { render } from 'enzyme';

import Page from './';

it('renders without crashing', () => {
    render(<Page>Test</Page>);
});