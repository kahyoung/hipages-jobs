import React from 'react';
import PropTypes from 'prop-types';

import './Icon.css';

const Icon = ({type}) => {
    return <i className={"icon-"+ type} />
};

Icon.propTypes = {
    type: PropTypes.oneOf(['menu', 'ellipsis-vert']).isRequired
};

export default Icon;