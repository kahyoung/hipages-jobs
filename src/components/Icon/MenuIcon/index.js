import React from 'react';

import Icon from '../';

const MenuIcon = () => (
    <Icon type='menu' />
);

export default MenuIcon;