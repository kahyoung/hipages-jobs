import React from 'react';

import Icon from '../';

const EllipsisVertIcon = () => (
    <Icon type='ellipsis-vert' />
);

export default EllipsisVertIcon;