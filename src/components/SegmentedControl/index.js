import React from 'react';
import PropTypes from 'prop-types';

import './SegmentedControl.css';

/**
 * A Segment Control contains a list of {@link Segment} elements, and presents them in a grouped manner
 *
 * Selecting a Segment will generally change the contents of the page to show different information
 */
const SegmentedControl = ({children}) => (
    <div className="segmented-control">
        {children}
    </div>
);

SegmentedControl.propTypes = {
    children: PropTypes.node.isRequired
};

export default SegmentedControl;