import React from "react";
import { render } from 'enzyme';

import SegmentedControl from './';
import Segment from './Segment';

it('renders without crashing', () => {
    render(<SegmentedControl><Segment>Test</Segment></SegmentedControl>);
});