import React from "react";
import { shallow } from 'enzyme';

import Segment from './';

it('renders without crashing', () => {
    shallow(<Segment>Test</Segment>);
});

it('has the active modifier when active prop is provided', () => {
    const segment = shallow(<Segment active={true}>Test</Segment>);
    expect(segment.hasClass('segmented-control__segment--active')).toBe(true);
});

it('can handle click events', () => {
    const onButtonClick = jest.fn();
    const segment = shallow(<Segment onClick={onButtonClick}>Test</Segment>);

    segment.simulate('click');
    expect(onButtonClick.mock.calls.length).toBe(1);
});