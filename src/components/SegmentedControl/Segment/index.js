import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';

import './Segment.css';

/**
 * A Segment is used in the {@link SegmentedControl} as the interactive component.
 *
 * Clicking a Segment will generally change the contents of the page to show specific content based on the Segment that is chosen
 */
const Segment = ({active, onClick, children}) => (
    <button className={classNames('segmented-control__segment', {'segmented-control__segment--active': active})} onClick={onClick}>
        {children}
    </button>
);

Segment.propTypes = {
    active: PropTypes.bool,
    children: PropTypes.node.isRequired,
    onClick: PropTypes.func
};

export default Segment;