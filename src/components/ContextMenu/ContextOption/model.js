/**
 * A model for the {@link ContextOption} component
 *
 * This is a basic model which has a text that is shown on the UI, as well as a fn which is called on click
 *
 * @constructor
 * @params {string} text - The text that will be displayed on the UI
 * @params {function} fn - The function to call on click
 */
class ContextOption {
    constructor(text, fn) {
        this.text = text;
        this.fn = fn;
    }
}

export default ContextOption;