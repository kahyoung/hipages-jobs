import React from 'react';
import PropTypes from 'prop-types';

import Link from '../../Link';
import ContextOptionModel from './model';

import './ContextOption.css';


const ContextOption = ({action}) => {
    return <Link className="context-option" onClick={action.fn}>{action.text}</Link>;
};

ContextOption.propTypes = {
    action: PropTypes.instanceOf(ContextOptionModel).isRequired
};

export default ContextOption;