import React from "react";
import { shallow } from 'enzyme';

import ContextOption from './';
import ContextOptionModel from './model';

it('renders without crashing', () => {
    let action = new ContextOptionModel('Test', jest.fn());
    shallow(<ContextOption action={action} />);
});

it('can call the provided onClick fn', () => {
    let mockCallback = jest.fn();
    let action = new ContextOptionModel('Test', mockCallback);

    let wrapper = shallow(<ContextOption action={action} />);
    wrapper.simulate('click');

    expect(mockCallback.mock.calls.length).toBe(1);
});