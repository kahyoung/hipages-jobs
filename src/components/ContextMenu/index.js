import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './ContextMenu.css';

import ContextOption from './ContextOption';
import ContextOptionModel from './ContextOption/model';

import Link from '../Link';
import EllipsisVertIcon from '../Icon/EllipsisVertIcon';

/**
 * Renders a popover menu with a list of actions that a user can click
 */
class ContextMenu extends Component {
    constructor(props) {
        super(props);

        this.state = {visible: false};

        this.closeMenu = this.closeMenu.bind(this);
        this.openMenu = this.openMenu.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);
    }

    /**
     * Remove any listeners when this component disappears
     */
    componentWillUnmount() {
        this.removeListeners();
    }

    /**
     * Hides the context menu
     */
    closeMenu(event) {
        // If an event is passed through, then it means this is being fired from the click off event
        // If that's the case, then we don't want to close the menu if we've clicked an element within this element
        // this._node is set via ref in the render function
        if (event && this._node && this._node.contains(event.target)) {
            return;
        }

        this.setState({visible: false});
        this.removeListeners();
    }

    /**
     * Opens the context menu
     */
    openMenu() {
        this.setState({visible: true});
        this.addListeners();
    }

    /**
     * Toggles the context menu
     */
    toggleMenu() {
        this.state.visible ? this.closeMenu() : this.openMenu();
    }

    /**
     * Adds required listeners to the document
     */
    addListeners() {
        document.addEventListener('click', this.closeMenu, false);
        document.addEventListener('touchstart', this.closeMenu, false);

    }

    /**
     * Removes any added listeners
     */
    removeListeners() {
        document.removeEventListener('click', this.closeMenu, true);
        document.removeEventListener('touchstart', this.closeMenu, true);
    }

    render() {
        let contextOptions = null;

        if (this.state.visible) {
            contextOptions = this.props.actions.map((action, index) => <ContextOption key={index} action={action} />);
            contextOptions = <div className="context-menu__options">{contextOptions}</div>;
        }

        return (
            <div className={classNames('context-menu', this.props.className)} ref={node => this._node = node}>
                <Link className="context-menu__button" onClick={this.toggleMenu}><EllipsisVertIcon/></Link>
                {contextOptions}
            </div>
        );
    }
}

ContextMenu.propTypes = {
    className: PropTypes.string,
    actions: PropTypes.arrayOf(PropTypes.instanceOf(ContextOptionModel)).isRequired
};

export default ContextMenu;