import React from 'react';
import { shallow } from 'enzyme';

import ContextMenu from './';

import ContextOption from './ContextOption';
import ContextOptionModel from './ContextOption/model';
import Link from '../Link';

it('renders without crashing', () => {
    const action = new ContextOptionModel('Test', jest.fn());
    shallow(<ContextMenu actions={[action]}/>);
});

it('renders a ContextOption for every action', () => {
   const actions = [
       new ContextOptionModel('Test', jest.fn()),
       new ContextOptionModel('Test', jest.fn()),
       new ContextOptionModel('Test', jest.fn()),
       new ContextOptionModel('Test', jest.fn())
   ];

   const wrapper = shallow(<ContextMenu actions={actions}/>);
   wrapper.find(Link).first().simulate('click');

   expect(wrapper.find(ContextOption).length).toBe(4);
});