import React from "react";
import { BrowserRouter as Router} from 'react-router-dom';
import { render } from 'enzyme';

import NavLink from './';

it('renders without crashing', () => {
    render(<Router><NavLink to="home">Home</NavLink></Router>);
});