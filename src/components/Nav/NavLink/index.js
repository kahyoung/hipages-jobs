import React from 'react';
import PropTypes from 'prop-types';
import { NavLink as RouterNavLink } from 'react-router-dom';

import './NavLink.css';
import './NavLink.medium.css';

/**
 * Renders a single link on the {@link Nav}
 *
 * Has a hard-dependency link to {@link Link} from `react-router-dom`, and thus requires this component to be a child of a {@link BrowserRouter}
 */
const NavLink = ({to, children}) => {
    return <RouterNavLink to={to} className="nav__link__link" activeClassName="nav__link__link--active">{children}</RouterNavLink>
};

NavLink.propTypes = {
    to: PropTypes.string.isRequired,
    children: PropTypes.string.isRequired
};

export default NavLink;