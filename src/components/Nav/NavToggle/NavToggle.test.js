import React from "react";
import { render, mount } from 'enzyme';

import NavToggle from './';
import Icon from '../../Icon';

it('renders without crashing', () => {
    render(<NavToggle />);
});

it('renders the menu icon', () => {
   let navToggle = mount(<NavToggle/>);
   expect(navToggle.find(Icon).length).toBe(1);
});