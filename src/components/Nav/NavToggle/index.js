import React from 'react';
import PropTypes from 'prop-types';

import './NavToggle.css';
import './NavToggle.medium.css';

import MenuIcon from '../../Icon/MenuIcon';

/**
 * Toggles the visibility of a [Nav's]{@link Nav} [Links}{@link NavLink}
 */
const NavToggle = ({onClick}) => (
    <button className="nav__toggle" onClick={onClick}>
        <MenuIcon/>
    </button>
);

NavToggle.propTypes = {
    onClick: PropTypes.func
};

export default NavToggle;