import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Nav.css';
import './Nav.medium.css';

import NavHeader from './NavHeader';
import NavToggle from './NavToggle';

/**
 * Renders the NavBar of the application, including the {@link NavHeader} and [NavLinks]{@link NavLink}
 */
class Nav extends Component {
    constructor(props) {
        super(props);

        this.state = {opened: false};

        this.toggleNav = this.toggleNav.bind(this);
    }

    componentWillUnmount() {
        this.removeListeners();
    }

    /**
     * Toggles the opening and closing of the NavBar
     */
    toggleNav(event) {
        if (event) {
            event.preventDefault();
        }

        let willBeOpened = !this.state.opened;

        this.setState((prevState) => ({
            opened: !prevState.opened
        }));

        if (willBeOpened) {
            this.addListeners();
        } else {
            this.removeListeners();
        }
    }

    /**
     * Adds event listeners to toggle the nav bar
     */
    addListeners() {
        document.addEventListener('click', this.toggleNav);
        document.addEventListener('touchend', this.toggleNav);
    }

    /**
     * Removes the event listeners to toggle the nav bar
     */
    removeListeners() {
        document.removeEventListener('click', this.toggleNav);
        document.removeEventListener('touchend', this.toggleNav);
    }

    render() {
        return (
            <nav className="nav">
                <div className="nav__content">
                    <NavHeader description="Australia's #1 site to hire tradespeople"/>
                    <NavToggle onClick={this.toggleNav}/>
                    <div className={classNames('nav__links', {'nav__links--opened': this.state.opened})}>
                        {this.props.children}
                    </div>
                </div>
            </nav>
        );
    }
}

Nav.propTypes = {
    children: PropTypes.node
};

export default Nav;