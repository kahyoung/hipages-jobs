import React from 'react';
import PropTypes from 'prop-types';

import logo from './logo.png';

import './NavHeader.css';

/**
 * Renders only the header of the Nav, which includes the Logo of the app as well as the description of the app
 */
const NavHeader = ({description}) => {
    let navDescription = null;

    if (description) {
        navDescription = <span className="nav__header__description">{description}</span>;
    }

    return (
        <div className="nav__header">
            <a className="nav__logo" href="/">
                <img src={logo} className="nav__logo__image" alt="hipages" />
                {navDescription}
            </a>
        </div>
    );
};

NavHeader.proptypes = {
    description: PropTypes.string
};

export default NavHeader;