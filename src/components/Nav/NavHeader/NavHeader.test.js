import React from "react";
import { render, shallow } from 'enzyme';

import NavHeader from './';

it('renders without crashing', () => {
    render(<NavHeader />);
});

it('can render a logo image', () => {
    let navHeader = shallow(<NavHeader />);
    expect(navHeader.find('img').length).toBe(1);
});

it('can render an optional description', () => {
    let navHeader = shallow(<NavHeader />);
    expect(navHeader.find('.nav__header__description').length).toBe(0);

    let navHeaderWithDescription = shallow(<NavHeader description="The best place for tests"/>);
    expect(navHeaderWithDescription.find('.nav__header__description').length).toBe(1);
});
