import React from "react";
import { BrowserRouter as Router} from 'react-router-dom';
import { render, mount } from 'enzyme';

import Nav from './';
import NavLink from './NavLink';


it('renders without crashing', () => {
    render(<Nav />);
});

it('can render a list of NavLinks', () => {
    let nav = mount((
        <Router>
            <Nav>
                <NavLink to='/home'>Home</NavLink>
                <NavLink to='/jobs'>Jobs</NavLink>
            </Nav>
        </Router>
    ));

    expect(nav.find(NavLink).length).toBe(2);
});
