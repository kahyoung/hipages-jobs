# Polyfills

Due to the varying feature-set across browsers, polyfills are sometimes required to implement core functions into Native 
object types that would be otherwise missing. For example, IE11 does not implement the `Array.prototype.find` function.

Each polyfill should only ever attempt to add the missing function _if the function does not already exist_.