import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch} from 'react-router-dom';

// Redux
import { Provider } from 'react-redux';
import store from './store';

// Components
import Nav from './components/Nav';
import NavLink from './components/Nav/NavLink';
import Footer from './components/Footer';
import FooterLink from './components/Footer/FooterLink';

// Modules
import Jobs from './modules/Jobs';
/**
 * This component is merely a holder for the app as a whole
 *
 * In order to modify any module (page) of the app, check out the 'modules' folder for the specific page you want to edit
 */
class App extends Component {
  render() {
    return (
        <Provider store={store}>
            <Router>
                <div className="App">
                    <Nav>
                        <NavLink to="/jobs">Jobs</NavLink>
                        <NavLink to="/friends">Friends</NavLink>
                        <NavLink to="/inspiration">Inspiration</NavLink>
                        <NavLink to="/contacts">Contacts</NavLink>
                    </Nav>
                    <Switch>
                        <Redirect exact from="/" to="/jobs" />
                        <Route path="/jobs" component={Jobs} />
                        <Redirect from="/friends" to="/jobs" />
                        <Redirect from="/inspiration" to="/jobs" />
                        <Redirect from="/contacts" to="/jobs" />
                    </Switch>
                    <Footer title="hipages.com.au © 2018">
                        <FooterLink to="https://www.homeimprovementpages.com.au/contactus">Customer Support</FooterLink>
                        <FooterLink to="https://www.homeimprovementpages.com.au/terms">Terms & Conditions</FooterLink>
                        <FooterLink to="https://www.homeimprovementpages.com.au/privacy">Privacy Policy</FooterLink>
                        <FooterLink to="https://www.homeimprovementpages.com.au/sitemap">Site Map</FooterLink>
                    </Footer>
                </div>
            </Router>
        </Provider>
    );
  }
}

export default App;
