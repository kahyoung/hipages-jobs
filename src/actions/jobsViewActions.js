const enums = {
    changeJobView: 'CHANGE_JOB_VIEW'
};

export function changeJobsView(view) {
    return {
        type: enums.changeJobView,
        payload: view
    }
}

export default enums;