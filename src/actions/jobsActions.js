import jobs from '../jobs.json';

const enums = {
    fetchJobs: 'FETCH_JOBS',
    fetchJobsPending: 'FETCH_JOBS_PENDING',
    fetchJobsFulfilled: 'FETCH_JOBS_FULFILLED',
    closeJob: 'CLOSE_JOB',
    openJob: 'OPEN_JOB'
};

export function fetchJobs() {
    return {
        type: enums.fetchJobsFulfilled,
        payload: jobs.jobs
    };
}

export function closeJob(job) {
    return {
        type: enums.closeJob,
        payload: job
    }
}

export function openJob(job) {
    return {
        type: enums.openJob,
        payload: job
    }
}

export default enums;