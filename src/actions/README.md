# Actions

This directory is where all Redux actions go.

Each action has a few exports:

- The default export is a list of enums, which is the `type` used in Redux. This is to keep enums consistent throughout 
the app. Keeping it centralised will also make renaming much easier, if ever need be
- You can also export individual functions, for example `fetchJobs`. This should primarily be done via object destructuring. 
Like so: `import { fetchJobs } from 'jobsActions'`.

For more information, see [Actions](https://redux.js.org/basics/actions).