# Home Improvement Pages - Jobs Prototype
This is a prototype of a design for the Home Improvement Pages' Web Application.

### Dependencies
The main dependency required is [Node](https://nodejs.org). Make sure you have the [latest version](https://nodejs.org/en/download/) 
of Node before running this.

The application itself is built with [react](https://reactjs.org), [redux](https://redux.js.org) and [react-redux](https://github.com/reactjs/react-redux) 
and makes use of [webpack](https://webpack.js.org).

All other dependencies are listed in `package.json`

### Installation
To get started, `cd` into the root directory and run: 

`npm install`

### Usage
To run the application for **development**, run: 

`npm start`

### Production
To build the project for **production**, run:

`npm build`

This creates a minified build in the `build` folder, to be deployed on any WebServer.

### Testing
Testing uses [Jest](https://facebook.github.io/jest/) and [enzyme](https://github.com/airbnb/enzyme) for rendering and 
simulating events.

You can run tests via:

`npm test`

### Environments
Developed on `MacOS 10.13.2 Node 9.6.1`

#### Compatible/tested environments
`MacOS`
- `Safari 11.0.2`
- `Chrome 64.0.3282.186`
- `Firefox 58.0.2`

`iPhone X - iOS 11.2.6`
- `Safari`
- `Chrome 65.0.3325.152`
- `Firefox 10.6`

`Windows 10`
- `Microsoft Edge`
- `Internet Explorer 11`
- `Chrome`

#### Incompatible/Untested environments
`Windows`
- `Internet Explorer < 11`


#### Discussion
Starting this project, I had 3 main goals. I wanted the prototype to be:
- Extensible - ready to scale with little extra effort
- Testable - able to not only test methods and data, but also being able to test the _result_ of user interactions
- Time efficient - the prototype should be able to be built relatively quickly, with a good foundation for future scaling 

I believe that these goals were achieved, mainly through the following decisions:
- Implementing React/Redux
- Testing with Enzyme
- Bootstrapping with Create React App 

##### React/Redux
I strongly believe in a component-driven development workflow. I believe that every element can be re-used, and 
that spending the time to code it right the first time can speed up the development of an application dramatically. 
Although it might seem overkill to implement the amount of components that I did for a prototype, I definitely saw the 
speed improvements once the components matured - and being able to test each component in isolation made the decision 
obvious to me.

A favourite example of mine is implementing the `No open/closed jobs` state.

I decided early on that each Job would exist in a `Card` component - a component that contained and presented a bunch 
of information nicely. After spending a bit of time to make sure that the component was fairly modular, it was amazing 
to see the component render with an actual list of items. The real magic was extending that component to create a `Card`
that was transparent and had a dashed border - the perfect placeholder layout for an empty message!

![Transparent Card](https://i.imgur.com/cKAjh9C.jpg "Transparent Card")

Redux is used quite sparingly in this prototype, but I believe that it was important to implement it for two main 
reasons. Firstly, it simplified the management of state in the Jobs page dramatically, which allowed me to focus more on 
the presentational aspects of the Job Page. Secondly, this need for state management would only grow with the 
application. Setting Redux early, and correctly, would leave the prototype in a very good condition for scaling.

##### Testing with Enzyme
Testing with Enzme made testing a *lot* safer.

The tests in this project not only cover specific Unit Tests for methods within each component, but it also includes 
user simulations. The best example is within the tests for the Job module. 

Rather than unit testing particular methods of the component, I elected to use enzyme's simulate methods to simulate 
actual user clicks, and to test on the result. This allows for much closer to real testing without the need for setting
up a Selenium grid (for now).

I would love to implement a robust testing suite, including full user flows with Selenium but elected not to. Firstly, 
my knowledge in Selenium/Selenium grid is not quite adept enough that I would be able to deliver a testing solution that
did more than what Enyme's `simulate` method does - and secondly, I felt that the size of the prototype *as is* did not 
warrant such a large configuration *at this time*.


##### Bootstrapping with Create React App

I chose to bootstrap the application with Create React App because of the ease in which it sets up a project (with a 
project structure that makes sense), and the amount of time that it saves. As well as this, the `eject` command makes 
the actual configuration of the project completely transparent.

I decided against `eject`ing this application because there was no need to, and the benefits of having 
[react-scripts](https://www.npmjs.com/package/react-scripts) as part of the project far outweighs the need to `eject`.

##### Other matters
###### Pixel perfection and responsive design
While I did my best to keep the prototype to match the designs pixel-for-pixel, there are sections that are off by a few 
pixels. I'm aware of this. The reason for this is because I decided to make use of `rem` and `em` measurement units 
rather than `px` units for most sizings. I believe that both `rem` and `em` units are much more powerful when it comes to 
both a responsive application, and an *accessible* application, as it not only handles the varying device sizes but also 
users' default text setting.

###### Sassy SCSS
I decided to use SCSS for accessibility (mixins for different browsers), as well as having a constant place for consistent 
elements (such as color). 

##### Thank you
I know this is a long README, but this project was a lot of fun for me to make. I hope it delivers to your standards :).